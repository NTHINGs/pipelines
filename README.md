# Pipelines

# Angular Pipeline
## Usage
https://gitlab.com/NTHINGs/pipelines/raw/latest/angular/.gitlab-ci.yml

## Required Variables
- HOST: IP or domain of the server to deploy
- USER: User for SSH.
- DEPLOY_DIR: Directory to upload the dist folder.
- PRIVATE_KEY: THIS IS DEFINED IN THE VARIABLES OF GITLAB. Private key to login in ssh

# Java Maven Pipeline
## Usage
https://gitlab.com/NTHINGs/pipelines/raw/latest/java-maven/.gitlab-ci.yml

## Required Variables
- HOST: IP or domain of the server to deploy
- USER: User for SSH.
- DEPLOY_DIR: Directory to upload the war file.
- PRIVATE_KEY: THIS IS DEFINED IN THE VARIABLES OF GITLAB. Private key to login in ssh
